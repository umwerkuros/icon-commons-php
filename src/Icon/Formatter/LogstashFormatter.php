<?php

namespace Icon\Formatter;

use Monolog\Formatter\NormalizerFormatter;
use Symfony\Component\HttpFoundation\Request;

class LogstashFormatter extends NormalizerFormatter
{
    /**
     * @var string application name
     */
    protected $applicationName;

    /**
     * @var string system hostname/ip
     */
    protected $systemName;

    /**
     * @param string $applicationName application name
     * @param string $systemName      system hostname/ip
     */
    public function __construct($applicationName, $systemName = null)
    {
        // logstash requires a ISO 8601 format date with optional millisecond precision.
        parent::__construct('Y-m-d\TH:i:s.uP');

        $this->applicationName = $applicationName;
        $this->systemName = $systemName ?: gethostname();
    }

    /**
     * {@inheritdoc}
     */
    public function format(array $record)
    {
        $record  = $this->normalize($record);
        $message = $this->formatIcon($record);

        return "SEPARATOR" . $this->toJson($message) . "\n";
    }

    /**
     * Return message prepared for logs.scaya-app.com
     *
     * @param  array $record
     *
     * @return array
     */
    protected function formatIcon(array $record)
    {
        if (empty($record['datetime'])) {
            $record['datetime'] = gmdate('c');
        }

        $message = array(
            '@timestamp' => $record['datetime'],
            '@version' => 1,
            'host' => $this->systemName,
            'icon_servicename' => $this->applicationName,
        );

        if (isset($record['level_name'])) {
            $message['priority'] = $record['level_name'];
        }

        if (isset($record['message'])) {
            $message['@message'] = $record['message'];
        }

        if (isset($record['extra']) && isset($record['extra']['icon_request'])) {
            foreach ($record['extra']['icon_request'] as $key => $value) {
                $message[$key] = $value;
            }
        }

        return $message;
    }
}
