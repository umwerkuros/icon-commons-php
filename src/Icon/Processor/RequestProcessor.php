<?php

namespace Icon\Processor;

use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Request;

class RequestProcessor
{
    /**
     * @var Request
     */
    private $request = null;

    /**
     * IconRequestProcessor constructor.
     *
     * @param RequestStack $request
     */
    public function __construct(RequestStack $request)
    {
        $this->request = $request->getCurrentRequest();
    }

    /**
     * Populate record extra field with request data
     *
     * @param array $record
     * @return array
     */
    public function processRequest(array $record)
    {
        if ($this->request && $this->request instanceof Request) {
            $record['extra']['icon_request'] = [
                "client"         => $this->request->getClientIp(),
                "port"           => $this->request->getPort(),
                "scheme"         => $this->request->server->get('SERVER_PROTOCOL'),
                "accept"         => implode(',', $this->request->getAcceptableContentTypes()),
                "request_method" => $this->request->getMethod(),
                "request"        => $this->request->getRequestUri(),
                "query_string"   => $this->request->getQueryString(),
                "scayaUid"       => $this->request->headers->get('X-Scaya-Uid'),
            ];
        }

        return $record;
    }
}
